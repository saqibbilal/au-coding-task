<?php

namespace App\Http\Controllers;

use App\Factories\MatrixFactory;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Helpers\ResponseHelper;
use Response;

class MatrixController extends BaseController
{
    use ResponseHelper;

    /**
     * Receives 2 matrices data as JSON and multiply them togather.
     *
     * @param Request $request The error message to be sent in response
     * @return object The universal response object
     */
    public function multiply(Request $request): object
    {
        
        try {
            if(!$request->isJson()) {
                return Response::json($this->errorResponse('Please provide a valid Json.'));
            }
            $json = $request->all();
            $matrices = $json;
            if(count($matrices) != 2) {
                return Response::json($this->errorResponse('Please provide data for 2 matrices.'));
            }
            $matrixA = MatrixFactory::create($matrices[0]);
            $matrixB = MatrixFactory::create($matrices[1]);
            $resultantMatrix = $matrixA->multiply($matrixB);
            $resultantMatrixExcel = $resultantMatrix->toExcel();
            return Response::json($this->successResponse('Matrices are multiplied successfully.', ['result'=>$resultantMatrixExcel]));
        } catch (\Exception $ex) {
            return Response::json($this->errorResponse($ex->getMessage()));
        }
    }
}
