<?php
namespace App\Exceptions;

use Exception;

final class IncompatibleMatrixException extends Exception
{
}
