<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Helpers\Excel;

class ExcelTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->excel = $this->getMockForTrait(Excel::class);
        $_alphabets = [];
        foreach (range('A', 'Z') as $char) {
            $_alphabets[] = $char;
        }
        $this->_alphabets = $_alphabets;
    }

    /**
     * @testCase test if the excel conversion is working fine
     */
    public function testExcelNameFromNumberTest()
    {
        $alphabets = [];
        for ($i=1; $i<=26; $i++) { 
            $alphabets[] = $this->excel->getExcelNameFromNumber($i);
        }
        $this->assertTrue($this->_alphabets == $alphabets);
    }

    /**
     * @testCase function throws \App\Exceptions\InvalidNumberException if the number of columns is not consistent
     */
    public function testInvalidNumberExceptionTest()
    {
        $this->expectException(\App\Exceptions\InvalidNumberException::class);
        $this->excel->getExcelNameFromNumber(0);
    }
    
}
