<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Factories\MatrixFactory;

class MatrixMultiplicationTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->SA = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
        ];

        $this->SB = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
        ];

        $this->SR = [
            [30, 36, 42],
            [66, 81, 96],
            [102, 126, 150],
        ];

        $this->identity = [
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1],
        ];
        $this->matrixSA = MatrixFactory::create($this->SA);
        $this->matrixSB = MatrixFactory::create($this->SB);
        $this->matrixSResultant = MatrixFactory::create($this->SR);
        $this->matrixIdentity = MatrixFactory::create($this->identity);

        $this->A = [
            [5, 3],
            [3, 6]
        ];

        $this->B = [
            [1],
            [2]
        ];

        $this->R = [
            [11],
            [15]
        ];
        $this->matrixA = MatrixFactory::create($this->A);
        $this->matrixB = MatrixFactory::create($this->B);
        $this->matrixResultant = MatrixFactory::create($this->R);
    }

    /**
     * @testCase Square Matrices Multiplication test
     */
    public function testSquareMatricesMultiplicationTest()
    {
        $resultantMatrix = $this->matrixSA->multiply($this->matrixSB);
        
        $this->assertTrue(
            $resultantMatrix->isEqual($this->matrixSResultant)
        );

        $this->assertTrue(
            $resultantMatrix->getRows() == $this->matrixSA->getRows()
        );

        $this->assertTrue(
            $resultantMatrix->getCols() == $this->matrixSB->getCols()
        );

        $this->assertTrue(
            $resultantMatrix->toExcel() == $this->matrixSResultant->toExcel()
        );
    }

    /**
     * @testCase Matrix multiplied with identity matrix will result in the same source matrix
     */
    public function testSquareMatrixMultiplicationByIdentityMatrixTest()
    {
        $resultantMatrix = $this->matrixSA->multiply($this->matrixIdentity);
        
        $this->assertTrue(
            $resultantMatrix->isEqual($this->matrixSA)
        );

        $this->assertTrue(
            $resultantMatrix->getRows() == $this->matrixSA->getRows()
        );

        $this->assertTrue(
            $resultantMatrix->getCols() == $this->matrixSA->getCols()
        );

        $this->assertTrue(
            $resultantMatrix->toExcel() == $this->matrixSA->toExcel()
        );
    }

    /**
     * @testCase Matrix multiplied with identity matrix will result in the same source matrix
     */
    public function testMatrixMultiplicationTest()
    {
        $resultantMatrix = $this->matrixA->multiply($this->matrixB);
        
        $this->assertTrue(
            $resultantMatrix->isEqual($this->matrixResultant)
        );

        $this->assertTrue(
            $resultantMatrix->getRows() == $this->matrixA->getRows()
        );

        $this->assertTrue(
            $resultantMatrix->getCols() == $this->matrixB->getCols()
        );

        $this->assertTrue(
            $resultantMatrix->toExcel() == $this->matrixResultant->toExcel()
        );
    }
    
}
